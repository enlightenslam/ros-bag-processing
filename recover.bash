rosbag_folder=$1  # dataset_folder_1
docker_image_name=$2  # ros_orb

base_path="$(pwd)/datasets/"
launch_path="$(pwd)/launch"
ip="$base_path$rosbag_folder"
rosbag_name=$(ls "$ip")
input_path="$base_path$rosbag_folder/$rosbag_name"
output_path=$(echo "$input_path" | cut -d'.' -f 1)
bag_file_name=$(basename "$output_path""_recovered.bag" | sed 's/.bag//')".bag"
txt_file_name=$(basename "$output_path" | sed 's/.bag//')"_mockup_trajectory.txt"

short=$(echo "${bag_file_name}" | cut -c1-2)
txt_file_name=$"$short""_gt.txt"

#echo "$rosbag_name"
#echo "$base_path"
#echo "$ip"
#echo "$launch_path"
#echo "$input_path"
#echo "$output_path"
#echo "$bag_file_name"
#echo "$txt_file_name"

to_pars=$(basename "$output_path".txt)
python main.py -model Recover \
	-topic "/camera/image_cropped" \
	-bag "$input_path" \
	-o "$ip" \
  -n "$bag_file_name"

python main.py -model MocapParser \
	-topic "/vicon/slam_camera_2/slam_camera_2" \
	-bag "$output_path""_recovered.bag" \
	-o "$ip" \
  -n "$txt_file_name"

sudo docker run --net=host --privileged --rm -it -v "$launch_path":/home/ros/launch \
	-v "$ip"/:/home/ros/datasets \
  -e DISPLAY=unix$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix "$docker_image_name"

./pars_orb.bash "$rosbag_folder" "$to_pars"