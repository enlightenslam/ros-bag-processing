## Timestamp recovering
For timestamp recovering its necessary to have prebuilt docker from: https://gitlab.com/enlightenslam/orb_slam_2_ros
1. Clone the repository into your catkin workspace:
```
git clone git@gitlab.com:enlightenslam/ros-bag-processing.git
```
2. Preparing the workspace:
```
cd ros-bag-processing
mkdir datasets
cd datasets
mkdir dataset_folder_1
cd ..
```
3. Preparing the data:

Переименуйте файл для обработки в _*'seq_number_method_number.bag'*_ и поместите
его в папку _*'dataset_folder_1'*_.
Примеры названий:

00_gt.bag

01_orig.bag

00_gan.bag

03_hist.bag

11_gamma2.bag

01_gamma4.bag

4. Recovering:

Предварительно необходимо разрешить исполнение скриптов:
```
chmod +x ./pars_orb.bash
chmod +x ./recover.bash
```
В случае, если не настроен автоматический запуск докер сервиса:
```
systemctl start docker
```
В качестве аргумента для bash скрипта необходимо указать два аргумента - 
название папки, где находится rosbag файл для обработки (e.g. dataset_folder_1) и
название образа докера (e.g. orb_ros):
```
./recover.bash dataset_folder_1 ros_orb
```
В случае правильных выполнений инструкций в терминале появится запрос пароля,
после чего вы попадётё в пространство докера.

5. Rosbag rerecording

После входа в докер необходимо дать разрешение на работу скрипта и запустить его:
```
chmod +x launch/record_rosbag.sh
./launch/record_rosbag.sh
```
После чего дождаться появляения сообщения:
```
[player-2] process finished cleanly
```
И завершить работу с докером последователь выполнив команды :
```
Ctrl+C
Crtl+D
```