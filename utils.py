import matplotlib.pyplot as plt
import cv2
import numpy as np


def plot_2_images(image_left, image_right, figsize=(20,15)):
    plt.figure(figsize=figsize)
    plt.subplot(1,2,1)
    plt.imshow(image_left)
    plt.subplot(1,2,2)
    plt.imshow(image_right)
    plt.show()
    

def get_rgb_hists(rgb_image, histSize=256, histRange=(0, 256), accumulate=False, hist_h=400):
    """
    histRange: the upper boundary is exclusive
    0 and hist_h: For this example, they are the lower and upper limits to normalize the values of histograms
    """

    rgb_planes = cv2.split(rgb_image)

    r_hist = cv2.calcHist(rgb_planes, [0], None, [histSize], histRange, accumulate=accumulate)
    g_hist = cv2.calcHist(rgb_planes, [1], None, [histSize], histRange, accumulate=accumulate)
    b_hist = cv2.calcHist(rgb_planes, [2], None, [histSize], histRange, accumulate=accumulate)

    #normalize the histogram so its values fall in the range indicated by the parameters entered:
    
    cv2.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
    cv2.normalize(g_hist, g_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
    cv2.normalize(r_hist, r_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
    
    return r_hist, g_hist, b_hist

def get_rgb_hist_image(r_hist, g_hist, b_hist, hist_w = 512, hist_h= 400, histSize=256):
    # Create an image from the histograms

    bin_w = int(round( hist_w/histSize ))
    histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)



    for i in range(1, histSize):
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(b_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(b_hist[i]) ),
                ( 255, 0, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(g_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(g_hist[i]) ),
                ( 0, 255, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(r_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(r_hist[i]) ),
                ( 0, 0, 255), thickness=2)
    return histImage


