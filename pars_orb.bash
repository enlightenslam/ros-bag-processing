rosbag_folder=$1  # dataset_folder_1
save_name=$2

base_path="$(pwd)/datasets/"
ip="$base_path$rosbag_folder"/
rosbag_path=$(find "$ip" -type f \( -iname "*.bag" ! -iname "*.txt*" ! -iname "*recovered*" ! -iname "*orig.*" ! -iname "*hist.*" ! -iname "*gan.*" ! -iname "*gamma2.*" ! -iname "*gamma4.*" ! -iname "*base.*" \))

bag_file_name=$(basename "$rosbag_path")
#new_file_name=$(echo "$bag_file_name" | sed 's/.bag//')".txt"

#echo "$rosbag_path"
#echo "$bag_file_name"
#echo "$ip"
#echo "$save_name"
python main.py -model OrbParser \
	-topic "/orb_slam2_mono/pose" \
	-bag "$rosbag_path" \
	-o "$ip" \
  -n "$save_name"
#python main.py -model OrbParser \
#	-topic "/orb_slam2_mono/pose" \
#	-bag "/home/andrei/git/VSLAM_articles/ros-bag-processing/datasets/seq_00_base/00_orig_recovered.bag" \
#	-o "/home/andrei/git/VSLAM_articles/ros-bag-processing/datasets/seq_00_base" \
#  -n "00_orig.txt"