import os
import sys
from pathlib import Path
from tqdm import tqdm
import rosbag


class MocapGtParser:
    def __init__(self, topics):
        self.topics = topics

    def parse(self, m):
        timestamp = m.message.header.stamp.to_sec()
        translation = m.message.transform.translation
        rotation = m.message.transform.rotation
        x = translation.x
        y = translation.y
        z = translation.z
        q1 = rotation.x
        q2 = rotation.y
        q3 = rotation.z
        q4 = rotation.w
        return True, f"{timestamp} {x} {y} {z} {q1} {q2} {q3} {q4}\n"


class OrbParser:
    def __init__(self, topics):
        self.topics = topics

    def parse(self, m):
        timestamp = m.message.header.stamp.to_sec()
        translation = m.message.pose.position
        rotation = m.message.pose.orientation
        x = translation.x
        y = translation.y
        z = translation.z
        q1 = rotation.x
        q2 = rotation.y
        q3 = rotation.z
        q4 = rotation.w
        return True, f"{timestamp} {x} {y} {z} {q1} {q2} {q3} {q4}\n"


def read_bag(file_path, type='r'):
    return rosbag.Bag(file_path, type)


def _check_dir(self, dir):
    print("Output will be saved in directory: {}".format(dir))
    if not os.path.exists(dir):
        os.mkdir(dir)


def parse_ros_bag(parser, bag_path, file_to_save):
    with rosbag.Bag(bag_path, 'r') as bag_to_read:
        with open(f'{file_to_save}', 'w') as parsed_ros_bag_file:
            for m in tqdm(bag_to_read.read_messages(topics=parser.topics)):
                ret, result = parser.parse(m)
                if ret:
                    parsed_ros_bag_file.write(f'{result}')


if __name__ == "__main__":
    parser = MocapGtParser("/vicon/slam_camera_2/slam_camera_2")
    parse_ros_bag(parser, sys.argv[1], sys.argv[2])

