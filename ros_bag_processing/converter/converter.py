import os
from pathlib import Path
import cv2
from tqdm import tqdm
import numpy as np
import rosbag
from cv_bridge import CvBridge


class Converter:
    def __init__(self):
        self.bridge = CvBridge()

    def _read_bag(self, file_path, type='r'):
        return rosbag.Bag(file_path, type)

    def _check_dir(self, dir):
        print("Output will be saved in directory: {}".format(dir))
        if not os.path.exists(dir):
            os.mkdir(dir)

    def bag2images(self, bag_path, img_topic, out_dir='output'):
        bag = self._read_bag(bag_path)

        self._check_dir(out_dir)

        cnt = 0
        for topic, msg, t in bag.read_messages(topics=[img_topic]):
            img = np.frombuffer(msg.data, dtype=np.uint8).reshape(msg.height, msg.width, -1)
            out_im_path = os.path.join(out_dir, '%06i.png' % cnt)
            cv2.imwrite(out_im_path, img)
            cnt += 1
            print('Wrote image: %i' % cnt)
        bag.close()

    def images2bag(self, img_dir, camera_topic, bag_to_save_name='bag_w_images.bag'):
        bag = self._read_bag(bag_to_save_name, 'w')
        for img_path in np.sort(os.listdir(img_dir)):
            img_path_full = os.path.join(img_dir, img_path)
            img = cv2.imread(img_path_full)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            msg = self.bridge.cv2_to_imgmsg(img)
            bag.write(camera_topic, msg)
        bag.close()

    def images2exisiting_bag(self, camera_topic, existing_bag_path, img_dir='output', bag_to_save_name='bag_w_images.bag'):
        bag_to_read = self._read_bag(existing_bag_path)
        bag_to_save = self._read_bag(bag_to_save_name, 'w')
        cnt = 0
        img_list = np.sort(os.listdir(img_dir))

        for topic, msg, time in bag_to_read.read_messages(topics=[camera_topic]):
            img_path = os.path.join(img_dir, img_list[cnt])
            img = cv2.imread(img_path)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            img_msgs = self.bridge.cv2_to_imgmsg(img)
            msg.data = img_msgs.data
            cnt += 1
            bag_to_save.write(topic, msg, t=time)

        bag_to_save.close()
        bag_to_read.close()

    def bag2model2bag(self, model, camera_topic, existing_bag_path, bag_to_save_name='processed.bag'):
        bag_to_read = self._read_bag(existing_bag_path)
        bag_to_save = self._read_bag(bag_to_save_name, 'w')
        for topic, msg, time in tqdm(bag_to_read.read_messages()):
            if topic == camera_topic:
                img = np.frombuffer(msg.data, dtype=np.uint8).reshape(msg.height, msg.width, -1)
                img = model.predict(img)

                img_msgs = self.bridge.cv2_to_imgmsg(img)
                msg.data = img_msgs.data
            bag_to_save.write(topic, msg, t=time)

        bag_to_save.close()
        bag_to_read.close()

    def recover_timestamps(self, camera_topic, existing_bag_path, bag_to_save_name='processed.bag'):
        bag_to_read = self._read_bag(existing_bag_path)
        bag_to_save = self._read_bag(bag_to_save_name, 'w')
        for topic, msg, time in tqdm(bag_to_read.read_messages()):
            if topic == camera_topic:

                msg.header.stamp = time

            bag_to_save.write(topic, msg, t=time)

        bag_to_save.close()
        bag_to_read.close()

        
if __name__ == "__main__":
    pass
