import sys


try:
    from enlighten_inference import EnlightenOnnxModel
except:
    print("Enable to import EnlightenGAN \n" 
    "Run following command: \n" 
    " pip install git+https://github.com/arsenyinfo/EnlightenGAN-inference")
    sys.exit()

import onnxruntime as rt
dev = rt.get_device()
if dev != 'GPU':
    print('Error: device is not GPU \n'
          'run following commands: \n'
          'pip uninstall -y onnxruntime \n'
          'pip install onnxruntime-gpu')
    sys.exit()

class EnlightenGanModel():
    def __init__(self):
        self.model = EnlightenOnnxModel()

    def predict(self, img):
        return self.model.predict(img)
        
