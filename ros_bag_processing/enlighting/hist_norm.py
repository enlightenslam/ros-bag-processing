import cv2
import numpy as np


class HistNormModel:
    def __init__(self):
        pass

    def predict(self,rgb_image):
        equalized_image_r = cv2.equalizeHist(rgb_image[:,:,0])
        equalized_image_g = cv2.equalizeHist(rgb_image[:,:,1])
        equalized_image_b = cv2.equalizeHist(rgb_image[:,:,2])
        equalized_image = np.concatenate((equalized_image_r[:,:,None], equalized_image_g[:,:,None], equalized_image_b[:,:,None]), axis=2)
        return equalized_image
