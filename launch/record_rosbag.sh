#!/bin/bash
source /home/ros/devel/setup.bash
bag_name=$(find /home/ros/datasets -type f \( -iname "*recovered*" ! -iname "*qqqqqqqqqqqqqq*" \))
#echo "$bag_name"
ROS_HOME='/home/ros/datasets' roslaunch launch/recovered.launch rosbag_name:="$bag_name"
rm -rf /home/ros/datasets/log
find /home/ros/datasets -name "rospack_cache*" -type f -delete
#find /home/ros/datasets -name "KeyFrame*" -type f -delete
