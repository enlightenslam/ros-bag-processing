from argparse import ArgumentParser
from pathlib import Path
import sys
from ros_bag_processing.enlighting.enlighten_gan import EnlightenGanModel
from ros_bag_processing.enlighting.hist_norm import HistNormModel
from ros_bag_processing.enlighting.gamma import GammaModel
from ros_bag_processing.converter.converter import Converter
from ros_bag_processing.parser.parser import MocapGtParser, parse_ros_bag, OrbParser

if __name__ == '__main__':
    parser = ArgumentParser(description='Process rosbag files')
    parser.add_argument('-model', help='model to process the images')
    parser.add_argument('-bag', help='rosbag file to process')
    parser.add_argument('-o', default=None, help='directory to save the output')
    parser.add_argument('-n', default=None, help='name of the resulting file')
    parser.add_argument('-gamma', default=2, help='gamma for the Gamma enlightening model')
    parser.add_argument('-topic', default='/camera/image_cropped', help='topic with images')

    args = parser.parse_args()
    bag_file_path = Path(args.bag)
    
    if not args.n:
        bag_to_save_name = bag_file_path.name[:-4] + "_processed.bag"
        if args.model == "Recover":
            bag_to_save_name = bag_file_path.name[:-4] + "_recovered.bag"
    else:
        bag_to_save_name = args.n

    if args.o is None:
        bag_to_save_name = bag_file_path.parent / bag_to_save_name
    else:
        bag_to_save_name = Path(args.o) / bag_to_save_name

    if args.model == "EnlightenGan":
        model = EnlightenGanModel()
    elif args.model == "HistNorm":    
        model = HistNormModel()
    elif args.model == "Gamma":
        model = GammaModel(float(args.gamma))
    elif args.model == 'Recover':
        cvt = Converter()
        cvt.recover_timestamps(args.topic, str(bag_file_path), bag_to_save_name=bag_to_save_name)
        sys.exit()
    elif args.model == "MocapParser":
        parser = MocapGtParser(args.topic)
        parse_ros_bag(parser, bag_file_path, bag_to_save_name)
        sys.exit()
    elif args.model == "OrbParser":
        parser = OrbParser(args.topic)
        parse_ros_bag(parser, bag_file_path, bag_to_save_name)
        sys.exit()
    
    print("It will take a while")
    cvt = Converter()
    cvt.bag2model2bag(model, args.topic, str(bag_file_path), bag_to_save_name=bag_to_save_name)
